/**
 * Poison in Your Veins.
 * 
 * @author @deconbatch
 * @version 0.1
 * Processing 3.2.1
 * created 2019.08.25
 * still images of Vector Field with parlin noise in angle.
 */

void setup() {

  size(980, 980);
  colorMode(HSB, 360, 100, 100, 100);
  rectMode(CENTER);
  smooth();

}

void draw() {

  int   frmMax  = 3;
  float hueInit = random(360.0);

  translate(width * 0.5, height * 0.5);

  for (int frmCnt = 1; frmCnt <= frmMax; frmCnt++) {

    hueInit += 120.0;
    noiseSeed(floor(hueInit));
  
    drawBackground(hueInit);
    drawCells(hueInit);
    casing();

    saveFrame("frames/" + String.format("%04d", frmCnt) + ".png");
  }
  
  exit();

}

/**
 * drawBackground : draw background
 */
private void drawBackground(float _hueInit) {

  background(_hueInit % 360.0, 15.0, 80.0, 100);
  noFill();
  for (int i = 0; i < width; i++) {
    float side = map(noise(i*0.001), 0.0, 1.0, -height, height);
    strokeWeight(1.0);
    stroke(0.0, 0.0, 80.0, 100.0);
    line(i - width * 0.5, side, i - width * 0.5, height * 0.5);
  }
  
}

/**
 * drawCells : draw cells
 */
private void drawCells(float _hueInit) {

  int cellCnt   = 5;
  int rippleCnt = floor(random(8.0, 18.0));
  
  int   ptnMax     = cellCnt * rippleCnt;
  int   plotMax    = floor(random(50.0, 150.0));
  float plotDiv    = 0.075;
  float noiseMult  = 0.4;
  float plotRadius = 0.25;
  float xPhase     = random(0.5);
  float yPhase     = random(0.5);

  noFill();
  for (int ptnCnt = 0; ptnCnt < ptnMax; ++ptnCnt) {
      
    float ptnRatio = map(ptnCnt, 0, ptnMax, 0.0, 1.0);

    float rPoint = 0.0;
    float xPoint = plotRadius * cos(TWO_PI * (xPhase + ptnRatio) * rippleCnt);
    float yPoint = plotRadius * sin(TWO_PI * (yPhase + ptnRatio) * rippleCnt);

    float baseHue = _hueInit + map(ptnRatio * ptnRatio, 0.0, 1.0, 0.0, 120.0);
    float baseSat = map(ptnRatio, 0.0, 1.0, 20.0, 80.0);
    float baseBri = map(ptnRatio * ptnRatio, 0.0, 1.0, 80.0, 30.0);
    float baseAlp = map(ptnRatio, 0.0, 1.0, 10.0, 50.0);
    float baseWgt = (1.0 - sin(HALF_PI * ptnRatio)) * 120.0;

    strokeWeight(baseWgt);
    stroke(baseHue % 360.0, baseSat, baseBri, baseAlp);

    beginShape();
    for (int plotCnt = 0; plotCnt < plotMax; ++plotCnt) {

      float plotRatio = map(plotCnt, 0, plotMax, 0.0, 1.0);

      float rPrev = rPoint;
      float xPrev = xPoint;
      float yPrev = yPoint;

      rPoint += noise(xPrev * noiseMult, yPrev * noiseMult);
      xPoint += plotDiv * cos(TWO_PI * rPoint);
      yPoint += plotDiv * sin(TWO_PI * rPoint);

      vertex(xPoint * width, yPoint * height);

    }
    endShape();
      
  }
}

/**
 * casing : draw fancy casing
 */
private void casing() {
  fill(0.0, 0.0, 0.0, 0.0);
  strokeWeight(54.0);
  stroke(0.0, 0.0, 60.0, 100.0);
  rect(0.0, 0.0, width, height);
  strokeWeight(50.0);
  stroke(0.0, 0.0, 100.0, 100.0);
  rect(0.0, 0.0, width, height);
  noStroke();
  noFill();
}
